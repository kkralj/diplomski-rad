import os


class DataSet:
    def __init__(self, dataset_root, batch_size, input_shape):
        self.batch_size = batch_size
        self.input_shape = input_shape

        shapes = [input_shape, []]

        self.train_dir = os.path.join(dataset_root, 'train')
        self.valid_dir = os.path.join(dataset_root, 'validate')
        self.test_dir = os.path.join(dataset_root, 'test')

import tensorflow as tf
import tensorflow.contrib.layers as layers

from models.vgg_pretrained import SequentialImagePoolingModel
from train_evaluate import freezed_pretrained_train_model

WEIGHT_DECAY = 5e-4
LEARNING_RATE = 1e-4
FULLY_CONNECTED = [200]
EPOCHS = 10
INPUT_SHAPE = [25, 140, 350, 3]


class FakeData:
    def __init__(self, batch_size):
        shapes = [batch_size, *INPUT_SHAPE]
        self.train_images = tf.zeros(shape=tuple(shapes), dtype=tf.float32)
        self.train_labels = tf.zeros(shape=(batch_size), dtype=tf.int32)

        # valid_images, = tf.zeros(shape=INPUT_SHAPE)
        # valid_labels = tf.zeros(shape=INPUT_SHAPE[0])
        # self.valid_images, self.valid_labels = tf.train.shuffle_batch(
        #     [valid_images, valid_labels], batch_size=batch_size, shapes=shapes)
        #
        # test_images, = tf.zeros(shape=INPUT_SHAPE)
        # test_labels = tf.zeros(shape=INPUT_SHAPE[0])
        # self.test_images, self.test_labels = tf.train.shuffle_batch(
        #     [test_images, test_labels], batch_size=batch_size, shapes=shapes)


dataset = FakeData(batch_size=10)

seq = SequentialImagePoolingModel(
    fully_connected_layers=FULLY_CONNECTED,
    dataset=dataset,
    weight_decay=WEIGHT_DECAY,
    vgg_init_dir=None,
    is_training=True
)

freezed_pretrained_train_model(seq, dataset, LEARNING_RATE, EPOCHS, '/tmp')

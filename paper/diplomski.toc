\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Problem prepoznavanja akcija}{2}{chapter.2}% 
\contentsline {chapter}{\numberline {3}Skupovi podataka}{4}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Skup podataka UCF-101}{4}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Skup podataka DeepMind Kinetics}{4}{section.3.2}% 
\contentsline {chapter}{\numberline {4}Naivni (baseline) model}{6}{chapter.4}% 
\contentsline {chapter}{\numberline {5}3D prostorno-vremenske konvolucijske neuronske mre\IeC {\v z}e}{7}{chapter.5}% 
\contentsline {chapter}{\numberline {6}Zaklju\IeC {\v c}ak}{9}{chapter.6}% 
\contentsline {chapter}{Literatura}{10}{chapter*.5}% 

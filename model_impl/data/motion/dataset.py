import random

from PIL import Image
import torch
from torch.utils.data import Dataset


class MotionDataSet(Dataset):
    def __init__(self, dic, in_channel, root_dir, mode, transform=None):
        self.keys, self.values = dic.keys(), dic.values()
        self.in_channel = in_channel
        self.root_dir = root_dir
        self.mode = mode
        self.transform = transform
        self.img_rows, self.img_cols = 224, 224

    def __len__(self):
        return len(self.keys)

    def __getitem__(self, index):
        if self.mode == 'train':
            self.video, nb_clips = self.keys[index].split(' ')
            self.clips_idx = random.randint(1, int(nb_clips))
        elif self.mode == 'val':
            self.video, self.clips_idx = self.keys[index].split(' ')
        else:
            raise ValueError('There are only train and val mode')

        label = self.values[index]
        label = int(label) - 1
        data = self.stackopf()

        if self.mode == 'train':
            sample = (data, label)
        elif self.mode == 'val':
            sample = (self.video, data, label)
        else:
            raise ValueError('There are only train and val mode')

        return sample

    def stackopf(self):
        name = 'v_' + self.video
        u = self.root_dir + 'u/' + name
        v = self.root_dir + 'v/' + name

        flow = torch.FloatTensor(2 * self.in_channel, self.img_rows, self.img_cols)
        i = int(self.clips_idx)

        for j in range(self.in_channel):
            idx = i + j
            idx = str(idx)
            frame_idx = 'frame' + idx.zfill(6)
            h_image = u + '/' + frame_idx + '.jpg'
            v_image = v + '/' + frame_idx + '.jpg'

            imgH = (Image.open(h_image))
            imgV = (Image.open(v_image))

            H = self.transform(imgH)
            V = self.transform(imgV)

            flow[2 * (j - 1), :, :] = H
            flow[2 * (j - 1) + 1, :, :] = V
            imgH.close()
            imgV.close()

        return flow

import pickle

from torch.utils.data import DataLoader
from torchvision import transforms

from data.motion.dataset import MotionDataSet
from data.splitter import UCFSplitter


class MotionDataLoader:
    def __init__(self, batch_size, num_workers, in_channel, data_path, ucf_list_dir, ucf_split_index, frame_count_path):
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.in_channel = in_channel
        self.data_path = data_path

        # split the training and testing videos
        splitter = UCFSplitter(path=ucf_list_dir, split_index=ucf_split_index)
        self.train_files, self.validation_files = splitter.split()

        self.frame_count = MotionDataLoader._load_frame_count(frame_count_path)

        self.train_frames = self._get_train_frame_count()
        self.validation_frames = self._get_validation_frame_count()

    @staticmethod
    def _load_frame_count(frame_count_path):
        print 'Loading frame count for each video'
        with open(frame_count_path, 'rb') as f:
            frame_count = {}

            frame_dict = pickle.load(f)
            for key, val in frame_dict.items():
                video_name = key.split('_', 1)[1].split('.', 1)[0]
                frame_count[video_name.lower()] = val

            return frame_count

    def _get_train_frame_count(self):
        frames = {}
        print '==> Generate frame numbers of each training video'

        for video, video_class in self.train_files.items():
            frame_cnt = self.frame_count[video.lower()] - 10 + 1
            frames[video + ' ' + str(frame_cnt)] = video_class

        return frames

    def _get_validation_frame_count(self):
        frames = {}
        print '==> Sampling validation frames'

        for video, video_class in self.validation_files.items():
            frame_cnt = self.frame_count[video.lower()] - 10 + 1
            interval = int(frame_cnt / 19)
            for i in range(19):
                frame = i * interval
                key = video + ' ' + str(frame + 1)
                frames[key] = video_class

        return frames

    def build(self):
        train_ld = self._get_train_loader()
        valid_ld = self._get_validation_loader()
        return train_ld, valid_ld, self.validation_files

    def _get_train_loader(self):
        training_set = MotionDataSet(
            dic=self.train_frames,
            in_channel=self.in_channel,
            root_dir=self.data_path,
            mode='train',
            transform=transforms.Compose([
                transforms.RandomCrop(224),
                transforms.ToTensor(),
            ])
        )
        print '==> Training data :', len(training_set)

        return DataLoader(
            dataset=training_set,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
            pin_memory=True
        )

    def _get_validation_loader(self):
        validation_set = MotionDataSet(
            dic=self.validation_frames,
            in_channel=self.in_channel,
            root_dir=self.data_path,
            mode='val',
            transform=transforms.Compose([
                transforms.RandomCrop(224),
                transforms.ToTensor(),
            ])
        )
        print '==> Validation data :', len(validation_set), ' frames'
        # print validation_set[1]

        return DataLoader(
            dataset=validation_set,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
        )


if __name__ == '__main__':
    data_loader = MotionDataLoader(
        batch_size=1,
        num_workers=1,
        in_channel=10,
        data_path='/mnt/home/kkralj/Downloads/tvl1_flow/',
        ucf_list_dir='../../ucfTrainTestlist/',
        ucf_split_index='01',
        frame_count_path='../frame_count/frame_count.pickle',
    )
    train_loader, val_loader, test_video = data_loader.build()

import pickle

from torch.utils.data import DataLoader
from torchvision import transforms

from data.spatial.dataset import SpatialDataSet
from data.splitter import UCFSplitter


class SpatialDataLoader:
    def __init__(self, batch_size, num_workers, data_path, frame_count_path, ucf_list_dir, ucf_split_index):
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.data_path = data_path

        # split the training and testing videos
        splitter = UCFSplitter(path=ucf_list_dir, split_index=ucf_split_index)
        self.train_files, self.validation_files = splitter.split()

        self.frame_count = SpatialDataLoader._load_frame_count(frame_count_path)

        self.train_frames = self._get_train_frame_count()
        self.validation_frames = self._get_validation_frame_count()

    @staticmethod
    def _load_frame_count(frame_count_path):
        print 'Loading frame count for each video'
        with open(frame_count_path, 'rb') as f:
            frame_count = {}

            frame_dict = pickle.load(f)
            for key, val in frame_dict.items():
                video_name = key.split('_', 1)[1].split('.', 1)[0]
                frame_count[video_name.lower()] = val

            return frame_count

    def _get_train_frame_count(self):
        frames = {}
        print '==> Getting frame counts of training videos'

        for video, video_class in self.train_files.items():
            frame_cnt = self.frame_count[video.lower()] - 10 + 1
            frames[video + ' ' + str(frame_cnt)] = video_class

        return frames

    def _get_validation_frame_count(self):
        frames = {}
        print '==> Sampling validation frames'

        for video, video_class in self.validation_files.items():
            frame_cnt = self.frame_count[video.lower()] - 10 + 1
            interval = int(frame_cnt / 19)
            for i in range(19):
                frame = i * interval
                key = video + ' ' + str(frame + 1)
                frames[key] = video_class

        return frames

    def build(self):
        train_ld = self._get_train_loader()
        valid_ld = self._get_validation_loader()
        return train_ld, valid_ld, self.validation_files

    def _get_train_loader(self):
        training_set = SpatialDataSet(
            data=self.train_frames,
            root_dir=self.data_path,
            mode='train',
            transform=transforms.Compose([
                transforms.RandomCrop(224),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
            ])
        )
        print '==> Training data :', len(training_set), 'frames'

        return DataLoader(
            dataset=training_set,
            batch_size=self.batch_size,
            shuffle=True,
            num_workers=self.num_workers,
        )

    def _get_validation_loader(self):
        validation_set = SpatialDataSet(
            data=self.validation_frames,
            root_dir=self.data_path,
            mode='val',
            transform=transforms.Compose([
                transforms.RandomCrop(224),
                transforms.ToTensor(),
                transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
            ])
        )
        print '==> Validation data :', len(validation_set), 'frames'

        return DataLoader(
            dataset=validation_set,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
        )


if __name__ == '__main__':
    data_loader = SpatialDataLoader(
        batch_size=1,
        num_workers=1,
        data_path='/mnt/home/kkralj/Downloads/jpegs_256/',
        frame_count_path='../frame_count/frame_count.pickle',
        ucf_list_dir='../../ucfTrainTestlist/',
        ucf_split_index='01',
    )
    train_loader, validation_loader, validation_files = data_loader.build()
    print train_loader, validation_loader

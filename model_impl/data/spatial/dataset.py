import random

from PIL import Image
from torch.utils.data import Dataset


class SpatialDataSet(Dataset):
    def __init__(self, data, root_dir, mode, transform):
        self.keys, self.values = data.keys(), data.values()
        self.root_dir = root_dir
        self.mode = mode
        self.transform = transform

    def __len__(self):
        return len(self.keys)

    def __getitem__(self, index):
        if self.mode == 'train':
            return self._get_train_item(index)

        elif self.mode == 'val':
            return self._get_validation_item(index)

        raise ValueError('There are only train and val mode, unknown mode: %s' % index)

    def _get_train_item(self, index, seq_len=15):
        video_name, frame_count = self.keys[index].split(' ')
        frame_count = int(frame_count)

        if frame_count - seq_len <= 0:
            raise Exception

        clips = []
        start_ind = random.randint(1, frame_count - seq_len)
        for i in range(0, seq_len):
            clips.append(start_ind + i)

        data = []
        for i in range(len(clips)):
            key = 'img' + str(i)
            clip_index = clips[i]
            data.append(self._load_image(video_name, clip_index))

        label = int(self.values[index]) - 1
        return data, label

    def _get_validation_item(self, index, seq_len=15):
        video_name, frame_count = self.keys[index].split(' ')
        frame_count = int(frame_count)

        if frame_count - seq_len <= 0:
            raise Exception

        clips = []
        start_ind = random.randint(1, frame_count - seq_len)
        for i in range(0, seq_len):
            clips.append(start_ind + i)

        data = []
        for i in range(len(clips)):
            key = 'img' + str(i)
            clip_index = clips[i]
            data.append(self._load_image(video_name, clip_index))

        label = int(self.values[index]) - 1
        return video_name, data, label

    def _load_image(self, video_name, index):
        path = self.root_dir + 'v_' + video_name + '/frame'
        img_path = path + ('%06d' % index) + '.jpg'

        with Image.open(img_path) as img:
            t = self.transform(img)
            return t

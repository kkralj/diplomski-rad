class UCFSplitter:
    def __init__(self, path, split_index):
        self.path = path
        self.split_index = split_index
        self.classes = self._load_classes()

    def _load_classes(self):
        classes = {}
        with open(self.path + 'classInd.txt') as f:
            for line in f.readlines():
                class_index, class_name = line.strip().split(' ')

                if class_name in classes:
                    raise Exception("duplicate class", class_name)
                classes[class_name] = class_index

        return classes

    def _load_file_names(self, path):
        file_names = {}

        with open(path) as f:
            for line in f.readlines():
                line = line.strip()

                action, rest = line.split('/', 1)
                video_name = rest.split(' ')[0].split('_', 1)[1].split('.', 1)[0]

                # if any(x in video_name for x in ['ApplyEyeMakeup', 'BlowingCandles', 'BrushingTeeth', 'JumpingJack']):
                if any(x in video_name for x in ['ApplyEyeMakeup']):
                    file_names[video_name] = int(self.classes[action])

        return file_names

    def split(self):
        train_list = self.path + 'trainlist' + self.split_index + '.txt'
        test_list = self.path + 'testlist' + self.split_index + '.txt'

        train_file_names = self._load_file_names(train_list)
        test_file_names = self._load_file_names(test_list)

        return train_file_names, test_file_names


if __name__ == '__main__':
    splitter = UCFSplitter(path='../ucfTrainTestlist/', split_index='01')
    train_file_names, test_file_names = splitter.split()
    print len(train_file_names), len(test_file_names)

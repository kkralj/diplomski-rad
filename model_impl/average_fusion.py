import pickle
import numpy as np
import torch

from data.spatial.data_loader import SpatialDataLoader
from utils import accuracy

if __name__ == '__main__':
    rgb_predictions = 'record/spatial/spatial_video_preds.pickle'
    with open(rgb_predictions, 'rb') as f:
        rgb = pickle.load(f)

    opf_predictions = 'record/motion/motion_video_preds.pickle'
    with open(opf_predictions, 'rb') as f:
        opf = pickle.load(f)

    dataloader = SpatialDataLoader(
        batch_size=1,
        num_workers=1,
        data_path='/mnt/home/kkralj/Downloads/ucf-orig/UCF-101/',
        ucf_list_dir='ucfTrainTestlist/',
        ucf_split_index='01',
        frame_count_path='data/frame_count/frame_count.pickle',
    )
    train_loader, val_loader, test_video = dataloader.build()

    video_level_preds = np.zeros((len(rgb.keys()), 101))
    video_level_labels = np.zeros(len(rgb.keys()))

    i, correct = 0, 0
    for name in sorted(rgb.keys()):
        r = rgb[name]
        o = opf[name]

        label = int(test_video[name]) - 1

        video_level_preds[i, :] = (r + o)
        video_level_labels[i] = label

        if np.argmax(r + o) == label:
            correct += 1

        i += 1

    video_level_labels = torch.from_numpy(video_level_labels).long()
    video_level_preds = torch.from_numpy(video_level_preds).float()

    top1, top5 = accuracy(video_level_preds, video_level_labels, top_k=(1, 5))

    top1 = top1.data.cpu().numpy()
    top5 = top5.data.cpu().numpy()

    print top1, top5

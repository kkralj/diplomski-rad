import argparse
import os
import pickle

import numpy as np

from tqdm import tqdm
import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.backends import cudnn
from torch.optim.lr_scheduler import ReduceLROnPlateau

from data.motion.data_loader import MotionDataLoader
from data.spatial.data_loader import SpatialDataLoader
from network import resnet18
from utils import accuracy

os.environ["CUDA_VISIBLE_DEVICES"] = "0"

parser = argparse.ArgumentParser()
parser.add_argument('--epochs', default=1, type=int)
parser.add_argument('--batch-size', default=20, type=int)
parser.add_argument('--lr', default=5e-4, type=float)


class MotionCNN:
    def __init__(self, epochs, lr, batch_size, train_loader, validation_loader, validation_files, channel):
        self.epochs = epochs
        self.lr = lr
        self.batch_size = batch_size
        self.channel = channel

        self.train_loader = train_loader
        self.validation_loader = validation_loader
        self.validation_files = validation_files

        self.best_prec_1 = 0

    def build_model(self):
        print ('==> Build model and setup loss and optimizer')

        # build model
        self.model = resnet18(pretrained=True, channel=self.channel).cuda()

        # Loss function and optimizer
        self.criterion = nn.CrossEntropyLoss().cuda()
        self.optimizer = torch.optim.SGD(self.model.parameters(), self.lr, momentum=0.9)
        self.scheduler = ReduceLROnPlateau(self.optimizer, 'min', patience=1, verbose=True)

    def run(self):
        self.build_model()
        cudnn.benchmark = True

        for self.epoch in range(self.epochs):
            self._train_epoch()
            prec_1, val_loss = self._validate_epoch()
            is_best = prec_1 > self.best_prec_1
            self.scheduler.step(val_loss)
            # save model
            if is_best:
                self.best_prec_1 = prec_1
                with open('record/motion/motion_video_preds.pickle', 'wb') as f:
                    pickle.dump(self.validation_predictions, f)

    def _train_epoch(self):
        print '==> Epoch:[{0}/{1}][training stage]'.format(self.epoch, self.epochs)

        # switch to train mode
        self.model.train()

        # mini-batch training
        for i, (images, label) in enumerate(tqdm(self.train_loader)):
            label = label.cuda(async=True)
            input_var = Variable(images).cuda()
            target_var = Variable(label).cuda()

            # compute output
            output = self.model(input_var)
            loss = self.criterion(output, target_var)

            # measure accuracy and record loss
            prec1, prec5 = accuracy(output.data, label, top_k=(1, 5))
            # log data

            # compute gradient and do SGD step
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

    def _validate_epoch(self):
        print('==> Epoch:[{0}/{1}][validation stage]'.format(self.epoch, self.epochs))

        # switch to evaluate mode
        self.model.eval()

        self.validation_predictions = {}

        with torch.no_grad():
            for i, (video_names, video_data, label) in enumerate(tqdm(self.validation_loader)):
                label = label.cuda(async=True)
                data_var = Variable(video_data).cuda(async=True)
                label_var = Variable(label).cuda(async=True)

                # compute output
                output = self.model(data_var)

                # calculate video level prediction
                predictions = output.data.cpu().numpy()

                data_count = predictions.shape[0]
                for j in range(data_count):
                    video_name = video_names[j]

                    if video_name not in self.validation_predictions:
                        self.validation_predictions[video_name] = predictions[j, :]
                    else:
                        self.validation_predictions[video_name] += predictions[j, :]

        top_1, top_5, loss = self._validation_accuracy()
        # save metrics
        return top_1, loss

    def _validation_accuracy(self):
        correct = 0
        video_predictions = np.zeros((len(self.validation_predictions), 101))
        video_predictions_labels = np.zeros(len(self.validation_predictions))

        i = 0
        for name in sorted(self.validation_predictions.keys()):
            predictions = self.validation_predictions[name]
            label = int(self.validation_files[name]) - 1

            video_predictions[i, :] = predictions
            video_predictions_labels[i] = label
            i += 1

            if np.argmax(predictions) == label:
                correct += 1

        video_predictions = torch.from_numpy(video_predictions).float()
        video_predictions_labels = torch.from_numpy(video_predictions_labels).long()

        top_1, top_5 = accuracy(video_predictions, video_predictions_labels, top_k=(1, 5))
        loss = self.criterion(Variable(video_predictions).cuda(), Variable(video_predictions_labels).cuda())

        top_1 = float(top_1.numpy())
        top_5 = float(top_5.numpy())
        # print correct

        print('* Video level Prec@1 {top_1:.3f}, Video level Prec@5 {top_5:.3f}'.format(top_1=top_1, top_5=top_5))
        return top_1, top_5, loss.data.cpu().numpy()


if __name__ == '__main__':
    global arg
    arg = parser.parse_args()

    data_loader = MotionDataLoader(
        batch_size=arg.batch_size,
        num_workers=8,
        in_channel=10,
        data_path='/mnt/home/kkralj/Downloads/tvl1_flow/',
        ucf_list_dir='ucfTrainTestlist/',
        ucf_split_index='01',
        frame_count_path='data/frame_count/frame_count.pickle'
    )
    train_loader, validation_loader, validation_files = data_loader.build()

    model = MotionCNN(
        epochs=arg.epochs,
        lr=arg.lr,
        batch_size=arg.batch_size,
        train_loader=train_loader,
        validation_loader=validation_loader,
        validation_files=validation_files,
        channel=2 * 10,
    )
    model.run()
